Linux Setup Bible
=================

This project aims to easy the installation of a new dev laptop by using ``Makefile`` command.

Note: You can run the ``Makefile`` command from any folder by using this following command: 
``make -C FOLDER_PATH COMMAND``

For example:
```
make -C ~/perso/dofavier/infra/Linux-Setup/ install_vscode_ext
```

To easy your use, you can add an alias to your ``.bashrc``:
```
alias cmake='make -C FOLDER_PATH'
```

Quelques alias utiles
=====================

```
# Search in this history
hy(){
   history | grep -vE -e "[0-9]{1,4}  hy |history" | grep -iE -e $1 | less
}
```

Usage: look for all ssh command in history: hy ssh


CREATE AN UBUNTU CONTAINER FOR TEST
===================================

```
docker build -t ubuntu:1 .
```

```
docker run -t -i ubuntu:1
```

Get the size of the containers

```
docker container ls -s -a
```

Delete the container 

```
docker rm ubuntu:1
```

Delete all the containers

```
docker container prune
```


PROPER INSTALL OF PYTHON & PYENV FROM ZERO
==========================================

Update the package
```
sudo apt-get update
```

Install basic python packages
```
sudo apt-get install wget curl git-all python3-pip python-is-python3 make build-essential python3-venv
```

Install additional packages to install within pyenv without any bug
```
sudo apt-get install build-essential zlib1g-dev libffi-dev libssl-dev libbz2-dev libreadline-dev libsqlite3-dev liblzma-dev
```

Install pyenv:
```
curl https://pyenv.run | bash
```

Add these lines to your .bashrc 
```
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
```

Source the .bashrc to take effect
```
source ~/.bashrc 
```

Install python 3.9.0 within pyenv 
```
pyenv install 3.9.0
```

Define as global 
```
pyenv global 3.9.0
```

Create a new virtualenv <my_env>
```
pyenv virtualenv 3.9.0 <my_env>
```


