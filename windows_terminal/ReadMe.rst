Install config for wsl
======================

1. Manually
------------

1. Put .wslconfig in %UserProfile%
2. Put settings.json in %LocalAppData%\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState

2. With Command line 
--------------------

With Powershell command line:

1. Download the necessary ressources

```
wget "https://gitlab.com/do-favier/infra/linux-setup/-/archive/main/linux-setup-main.zip?path=windows_terminal" -outfile "wsl.zip"
```

2. Unzip the files

```
Expand-Archive "wsl.zip"
```

3. Go to the folder

```
cd wsl\linux-setup-main-windows_terminal\windows_terminal
```

3. Put .wslconfig in %UserProfile%

```
Copy-Item -Path .\.wslconfig -Destination "$env:UserProfile"
```

2. Put settings.json in %LocalAppData%\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState

```
Copy-Item -Path .\settings.json -Destination "$env:LocalAppData\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState"
```

3. In one command line 
----------------------

wget "https://gitlab.com/do-favier/infra/linux-setup/-/archive/main/linux-setup-main.zip?path=windows_terminal" -outfile "wsl.zip"
Expand-Archive "wsl.zip"
cd wsl\linux-setup-main-windows_terminal\windows_terminal
Copy-Item -Path .\.wslconfig -Destination "$env:UserProfile"
Copy-Item -Path .\settings.json -Destination "$env:LocalAppData\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState"
